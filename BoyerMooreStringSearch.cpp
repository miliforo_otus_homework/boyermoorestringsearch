#include <iostream>
#include <map>
#include <string>
#include <vector>
using namespace std;

class StringSearch
{
public:
    int FullSearch(string SearchIn, string Substring)
    {
        int SymbolIndex = 0;
        int SearchArea = static_cast<int>(SearchIn.length() - Substring.length());
        while (SymbolIndex <= SearchArea)
        {
            int Counter = static_cast<int>(Substring.length()) - 1;

            while (Counter >= 0 && SearchIn[SymbolIndex + Counter] == Substring[Counter])
            {
                Counter--;
            }

            if (Counter == -1)
            {
                return SymbolIndex;
            }

            SymbolIndex++;
        }

        return -1;
    }

    void CreateShift(vector<int>& ShiftArray, string Substring)
    {
        const int Size = 128;
        const int SubstringSize = static_cast<int>(Substring.length());
        ShiftArray.resize(Size, SubstringSize);

        for (int i = 0; i < SubstringSize - 1; i++)
        {
            ShiftArray[Substring[i]] = 1;
        }
    }

    int ShiftSeach(string SearchIn, string Substring)
    {
        vector<int> Shift;
        CreateShift(Shift, Substring);

        int SymbolIndex = 0;
        int SearchArea = static_cast<int>(SearchIn.length() - Substring.length());
        while (SymbolIndex <= SearchArea)
        {
            int Counter = static_cast<int>(Substring.length()) - 1;

            while (Counter >= 0 && SearchIn[SymbolIndex + Counter] == Substring[Counter])
            {
                Counter--;
            }

            if (Counter == -1)
            {
                return SymbolIndex;
            }

            SymbolIndex += Shift[SearchIn[SymbolIndex + Substring.length() - 1]];
        }

        return -1;
    }

    int BoyerMooreSearch(string SearchIn, string Substring)
    {
        int TextLength = static_cast<int>(SearchIn.length());
        int MaskLength = static_cast<int>(Substring.length());

        vector<int> Shift;
        CreateShift(Shift, Substring);

        int SuffixShift = MaskLength - 1, Counter = SuffixShift, SymbolIndex = SuffixShift;

        while (Counter >= 0 && SuffixShift <= TextLength - 1)
        {
            Counter = MaskLength - 1;
            SymbolIndex = SuffixShift;

            while (Counter >= 0 && SearchIn[SymbolIndex] == Substring[Counter])
            {
                SymbolIndex--;
                Counter--;
            }

            SuffixShift += Shift[SuffixShift];
        }

        if (SymbolIndex >= TextLength - MaskLength)
        {
            return -1;
        }
        return SymbolIndex + 1;
    }
};


int main(int argc, char* argv[])
{
    string Text = "STRONGSTRING";
    string Mask = "STRING";
    auto SearchRef = new StringSearch;

    cout << SearchRef->FullSearch(Text, Mask) << "\n";
    cout << SearchRef->ShiftSeach(Text, Mask) << "\n";
    cout << SearchRef->BoyerMooreSearch(Text, Mask);
    return 0;
}
